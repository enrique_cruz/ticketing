CREATE DATABASE  IF NOT EXISTS `ticketing` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `ticketing`;
-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: ticketing
-- ------------------------------------------------------
-- Server version	5.7.31-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `estatus`
--

DROP TABLE IF EXISTS `estatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `estatus` (
  `idestatus` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idestatus`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `evidenciasticket`
--

DROP TABLE IF EXISTS `evidenciasticket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `evidenciasticket` (
  `idEvidencia` int(11) NOT NULL AUTO_INCREMENT,
  `nombreArchivo` varchar(150) DEFAULT NULL,
  `pathArchivo` varchar(165) DEFAULT NULL,
  `fechaDeSubida` datetime DEFAULT NULL,
  `idTicket` int(11) DEFAULT NULL,
  `descripcion` varchar(200) DEFAULT NULL,
  `evidenciaAgente` varchar(45) DEFAULT '0',
  PRIMARY KEY (`idEvidencia`)
) ENGINE=InnoDB AUTO_INCREMENT=3541 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gastostickets`
--

DROP TABLE IF EXISTS `gastostickets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gastostickets` (
  `idGastoTicket` int(11) NOT NULL AUTO_INCREMENT,
  `idTicket` int(11) DEFAULT NULL,
  `concepto` varchar(100) DEFAULT NULL,
  `cantidad` float DEFAULT NULL,
  `pagado` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`idGastoTicket`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ingenierosflotantes`
--

DROP TABLE IF EXISTS `ingenierosflotantes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ingenierosflotantes` (
  `idingenieroflotante` int(11) NOT NULL AUTO_INCREMENT,
  `curp` varchar(20) DEFAULT NULL,
  `nombre` varchar(80) DEFAULT NULL,
  `apPaterno` varchar(80) DEFAULT NULL,
  `apMaterno` varchar(80) DEFAULT NULL,
  `telefono` varchar(10) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `estado` varchar(100) DEFAULT NULL,
  `municipio` varchar(100) DEFAULT NULL,
  `calle` varchar(100) DEFAULT NULL,
  `numeroExterior` varchar(10) DEFAULT NULL,
  `numeroInterior` varchar(10) DEFAULT NULL,
  `colonia` varchar(100) DEFAULT NULL,
  `cp` int(11) DEFAULT NULL,
  `pass` varchar(100) DEFAULT NULL,
  `fechaRegistro` datetime NOT NULL,
  `empresa` varchar(20) DEFAULT NULL,
  `proyectoIngreso` varchar(200) DEFAULT NULL,
  `estatus` int(11) DEFAULT '0',
  `edad` int(11) DEFAULT NULL,
  `genero` varchar(1) DEFAULT NULL,
  `clabeInterbancaria` varchar(30) DEFAULT NULL,
  `banco` varchar(45) DEFAULT NULL,
  `numeroCuenta` varchar(45) DEFAULT NULL,
  `numeroTarjeta` varchar(20) DEFAULT NULL,
  `comentarios` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`idingenieroflotante`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notasticket`
--

DROP TABLE IF EXISTS `notasticket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `notasticket` (
  `idNotaTicket` int(11) NOT NULL AUTO_INCREMENT,
  `nota` varchar(1000) DEFAULT NULL,
  `idTicket` int(11) NOT NULL,
  `fechaNota` datetime DEFAULT NULL,
  `tipoNota` tinyint(4) DEFAULT NULL,
  `creadaAgente` int(11) DEFAULT '0',
  PRIMARY KEY (`idNotaTicket`)
) ENGINE=InnoDB AUTO_INCREMENT=2343 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `personasdatos`
--

DROP TABLE IF EXISTS `personasdatos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `personasdatos` (
  `idPersonaDatos` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `apPaterno` varchar(50) DEFAULT NULL,
  `apMaterno` varchar(50) DEFAULT NULL,
  `telefono` varchar(10) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `estado` varchar(45) DEFAULT NULL,
  `municipio` varchar(45) DEFAULT NULL,
  `calle` varchar(45) DEFAULT NULL,
  `numExterior` int(11) DEFAULT NULL,
  `numInterior` varchar(10) DEFAULT NULL,
  `colonia` varchar(50) DEFAULT NULL,
  `cp` int(5) DEFAULT NULL,
  PRIMARY KEY (`idPersonaDatos`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `prioridadproyecto`
--

DROP TABLE IF EXISTS `prioridadproyecto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `prioridadproyecto` (
  `idPriporidadProyecto` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` int(11) DEFAULT NULL,
  `tiempoVida` int(11) DEFAULT NULL,
  `idProyecto` int(11) NOT NULL,
  PRIMARY KEY (`idPriporidadProyecto`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `proyectocuenta`
--

DROP TABLE IF EXISTS `proyectocuenta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `proyectocuenta` (
  `idproyectoCuenta` int(11) NOT NULL AUTO_INCREMENT,
  `nombreProyecto` varchar(45) DEFAULT NULL,
  `lider` int(11) DEFAULT NULL,
  `fechaInicioPlaneacion` datetime DEFAULT NULL,
  `fechaInicioEjecucion` datetime DEFAULT NULL,
  `fechaFinPlaneacion` datetime DEFAULT NULL,
  `fechaFinEjecucion` datetime DEFAULT NULL,
  `costoTotalPlaneacion` double DEFAULT NULL,
  `costoTotalEjecucion` double DEFAULT NULL,
  `IngresoTotalPlaneacion` double DEFAULT NULL,
  `IngresoTotalEjecucion` double DEFAULT NULL,
  `utilidadEjecucion` double DEFAULT NULL,
  `utilidadPlaneacion` double DEFAULT NULL,
  `utilidadPorcentajePlaneacion` double DEFAULT NULL,
  `utilidadPorcentajeEjecucíon` double DEFAULT NULL,
  `diashoras` varchar(20) DEFAULT '0111110|07:00,19:00',
  PRIMARY KEY (`idproyectoCuenta`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `prueba`
--

DROP TABLE IF EXISTS `prueba`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `prueba` (
  `idprueba` int(11) NOT NULL AUTO_INCREMENT,
  `prueba` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idprueba`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ticket`
--

DROP TABLE IF EXISTS `ticket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ticket` (
  `idticket` int(11) NOT NULL AUTO_INCREMENT,
  `fechaCreacion` varchar(45) DEFAULT NULL,
  `fechaServicio` datetime DEFAULT NULL,
  `asunto` varchar(200) DEFAULT NULL,
  `prioridad` int(11) DEFAULT NULL,
  `descripcion` varchar(1000) DEFAULT NULL,
  `idTipoTicket` int(11) NOT NULL,
  `idEstatus` int(11) NOT NULL,
  `idAgente` int(11) NOT NULL DEFAULT '0',
  `idCreador` int(11) NOT NULL,
  `idProyecto` int(11) DEFAULT NULL,
  `nombreSolicitante` varchar(150) DEFAULT NULL,
  `correoContacto` varchar(150) DEFAULT NULL,
  `cp` int(11) DEFAULT NULL,
  `Estado` varchar(100) DEFAULT NULL,
  `municipio` varchar(100) DEFAULT NULL,
  `asentamiento` varchar(200) DEFAULT NULL,
  `calle` varchar(100) DEFAULT NULL,
  `numeroExterior` int(11) DEFAULT NULL,
  `numeroInterior` varchar(5) DEFAULT NULL,
  `tipoAgente` int(11) DEFAULT '0',
  `folio` varchar(45) DEFAULT NULL,
  `lat` double DEFAULT '0',
  `lng` double DEFAULT '0',
  `pais` varchar(150) DEFAULT 'México',
  `tiempoUrgente` int(11) DEFAULT '0',
  PRIMARY KEY (`idticket`)
) ENGINE=InnoDB AUTO_INCREMENT=1641 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tipoticket`
--

DROP TABLE IF EXISTS `tipoticket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tipoticket` (
  `idtipoticket` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idtipoticket`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tipousuario`
--

DROP TABLE IF EXISTS `tipousuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tipousuario` (
  `idtipoUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombreTipo` varchar(45) DEFAULT NULL,
  `descripcion` varchar(150) DEFAULT NULL,
  `accesos` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idtipoUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(45) DEFAULT NULL,
  `pass` varchar(45) DEFAULT NULL,
  `idPersonaDatos` int(11) NOT NULL,
  `idTipoUsuario` int(11) NOT NULL,
  PRIMARY KEY (`idusuario`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping events for database 'ticketing'
--

--
-- Dumping routines for database 'ticketing'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-01-27 13:14:27
